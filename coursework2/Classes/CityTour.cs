﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2.Classes
{

    //This class inherits all the properties from the Journey class but has a different type of cost
    public class CityTour : Journey
    {
        private static int COST = 20;

        public CityTour() { }
        public CityTour(
            string pickupDescription, 
            int pickupArea, 
            DateTime pickupDate, 
            string destinationDescription, 
            int destinationArea, 
            string customerName, 
            string customerAddress,
            int id
            )
        {

            this.PickupDescription = pickupDescription;
            this.PickupArea = pickupArea;
            this.PickupDate = pickupDate;
            this.DestinationDescription = destinationDescription;
            this.DestinationArea = destinationArea;
            this.CustomerName = customerName;
            this.CustomerAddress = customerAddress;
            this.JourneyId = id;
        }
            
        //returns the cost based on the area code
        public int TourCost()
        {
            if (PickupArea >= 5 && PickupArea <= 20)
                return COST + 10;
            if (PickupArea >= 21 && PickupArea <= 22)
                return COST + 15;

            return COST;
        }
    }
}
