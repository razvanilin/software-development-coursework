﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Coursework2
{
    //This class is used to do all the logic of the Vehicle Window
    class VehicleHandler
    {
        private List<Vehicle> savedVehicles = new List<Vehicle>();
        private Serializer serializer = new Serializer();

        private bool savedChanges;

        private int vehicleId = 0;

        //the constructor calls the deserialization method from the Serializer class and saves the data into a list
        //it also keeps track of the id by getting the id of the last item in the list
        public VehicleHandler()
        {
            savedVehicles = serializer.deserializeVehicle("vehicle.xml");
            if (savedVehicles.Count != 0)
                vehicleId = savedVehicles[savedVehicles.Count - 1].Id;

            serializer.closeFiles();
            savedChanges = true;
        }

        //Method used to assign values to the vehicle objects
        //if everything goes without a problem, a new vehicle is created and added to the list
        public void setFields(string registration)
        {
            int validator = 0;

            if (string.IsNullOrWhiteSpace(registration)){
                validator++;
                MessageBox.Show("Input something in the registration field.");
            }

            if (validator==0 )
            {
                vehicleId++;
                savedVehicles.Add(new Vehicle(vehicleId, registration));
                savedChanges = false;
            }
        }

        //Method used to populate the combo box with all the vehicles in the list, using the internal cmbBoxSummary method
        public void populateComboBox(ComboBox vehicleBox)
        {
            vehicleBox.Items.Clear();
            foreach (Vehicle vehicle in savedVehicles)
            {
                vehicleBox.Items.Add(vehicle.cmbBoxSummary());
            }
        }

        //Method used to get the id of the vehicle from the selected item in the combo box
        public int getIdFromString(string substr)
        {
            string number = "";
            for (int i = 0; i < substr.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(substr.Substring(i, 1)))
                    break;
                number = number + substr.Substring(i, 1);
            }
            try
            {
                return int.Parse(number);
            }
            catch { MessageBox.Show("Fix the id from string problem! " + number); }

            return 0;
        }

        // Method used to call the serialization from the Serializer class with the given list
        public void serializeVehicle()
        {
            serializer.serializeVehicle("vehicle.xml", savedVehicles);
        }

        //Method used to get the details of the vehicle based on the selected item in the combobox
        public Vehicle getObject(int id)
        {
            foreach (Vehicle vehicle in savedVehicles)
            {
                if (vehicle.Id == id)
                    return vehicle;
            }
            return new Vehicle();
        }

        //A getter for the vehicle list
        public List<Vehicle> SavedVehicles
        {
            get { return savedVehicles; }
        }

        //a getter for the savedChanges variable used to check if a modification was made and not saved
        public bool SavedChanges
        {
            get { return savedChanges; }
            set { savedChanges = value; }
        }
    }
}
