﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using Coursework2.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Coursework2
{
    //This class is used to do all the serialization logic so in order to get or write information,
    //the other classes will use the methods written here
    class Serializer
    {

        private XmlSerializer journeySerializer = new XmlSerializer(typeof(List<JourneyEntity>));
        private XmlSerializer tourSerializer = new XmlSerializer(typeof(List<TourEntity>));
        private XmlSerializer vehicleSerializer = new XmlSerializer(typeof(List<Vehicle>));

        private StreamWriter journeyWriter;
        private StreamWriter tourWriter;
        private StreamWriter vehicleWriter;

        private FileStream journeyReader;
        private FileStream tourReader;
        private FileStream vehicleReader;

        private Dictionary<Journey, Vehicle> journeyDictionary = new Dictionary<Journey,Vehicle>();
        private Dictionary<CityTour, Vehicle> tourDictionary = new Dictionary<CityTour, Vehicle>();

        //This method deserializez the files and then it transfers all the information into a dictionary
        //The list of entities is created as a workaround for the XmlSerializer incompatibility with dictionaries
        public Dictionary<Journey, Vehicle> deserializeJourney(string path)
        {
            if (File.Exists(path))
            {
                List<JourneyEntity> journeyEntities = new List<JourneyEntity>();
                journeyReader = new FileStream(path, FileMode.Open);
                if (journeyReader.Length != 0)
                    journeyEntities = (List<JourneyEntity>)journeySerializer.Deserialize(journeyReader);

                foreach (JourneyEntity entity in journeyEntities)
                {
                    journeyDictionary.Add(entity.Key, entity.Value);
                }

                return journeyDictionary;
            }

            return new Dictionary<Journey, Vehicle>();
        }

        //This method deserializez the files and then it transfers all the information into a dictionary
        public Dictionary<CityTour, Vehicle> deserializeTour(string path)
        {
            tourDictionary = new Dictionary<CityTour,Vehicle>();
            if (File.Exists(path))
            {
                List<TourEntity> tourEntities = new List<TourEntity>();
                tourReader = new FileStream(path, FileMode.Open);
                if (tourReader.Length != 0)
                    tourEntities = (List<TourEntity>)tourSerializer.Deserialize(tourReader);

                foreach (TourEntity entity in tourEntities)
                {
                    tourDictionary.Add(entity.Key, entity.Value);
                }

                return tourDictionary;
            }

            return new Dictionary<CityTour, Vehicle>();
        }

        //This method deserializes the file and returns the list that is generated during the process
        public List<Vehicle> deserializeVehicle(string path)
        {
            if (File.Exists(path))
            {
                vehicleReader = new FileStream(path, FileMode.Open);
                if (vehicleReader.Length != 0)
                    return (List<Vehicle>)vehicleSerializer.Deserialize(vehicleReader);
                else
                    return new List<Vehicle>();
            }
            else
                return new List<Vehicle>();
        }

        //THis method serializes the data that is passed to it
        //In order to be able to store all the information and keeping the refferences, all the data from the dictionaries
        //is passed to the list of entities
        public void serializeJourney(string path, Dictionary<Journey, Vehicle> dictionary)
        {
            journeyWriter = new StreamWriter(path);
            List<JourneyEntity> journeyEntities = new List<JourneyEntity>(dictionary.Count);

            foreach (Journey journey in dictionary.Keys)
            {
                journeyEntities.Add(new JourneyEntity(journey, dictionary[journey]));
            }
            journeySerializer.Serialize(journeyWriter, journeyEntities);
            journeyWriter.Close();
        }

        //This method serializes the data that is passed to it
        //In order to be able to store all the information and keeping the refferences, all the data from the dictionaries
        //is passed to the list of entities
        public void serializeTour(string path, Dictionary<CityTour, Vehicle> dictionary)
        {
            tourWriter = new StreamWriter(path);
            List<TourEntity> tourEntities = new List<TourEntity>(dictionary.Count);

            foreach (CityTour tour in dictionary.Keys)
            {
                tourEntities.Add(new TourEntity(tour, dictionary[tour]));
            }
            tourSerializer.Serialize(tourWriter, tourEntities);
            tourWriter.Close();
        }

        //This method serializez the list that is passed to it into a file
        public void serializeVehicle(string path, List<Vehicle> list)
        {
            vehicleWriter = new StreamWriter(path);
            vehicleSerializer.Serialize(vehicleWriter, list);
            vehicleWriter.Close();
        }

        //This method closes all the files when it's called
        //It is used to avoid errors while using multiple files in multiple places
        public void closeFiles()
        {
            if (journeyReader != null)
                journeyReader.Close();
            if (tourReader != null)
                tourReader.Close();
            if (vehicleReader != null)
                vehicleReader.Close();
        }
    }
}
