﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2.Classes
{
    //Class used as a workaround to the incompatibility of XML serialization of Dictionaries
    //This entity will work as a Dictionary. It has two fields for keys and values
    //Instead of serializing a dictionary I will serialize a list of these objects
    //When deserializing I will transfer all the information from the list to a dictionary for easy handling
    public class TourEntity
    {

        public CityTour Key;
        public Vehicle Value;

        public TourEntity() { }

        public TourEntity(CityTour key, Vehicle value)
        {
            Key = key;
            Value = value;
        }
    }
}
