﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using Coursework2.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Coursework2
{
    //Class used to do the logic of the Journey window.
    class JourneyHandler
    {
        private List<Journey> savedJourneys = new List<Journey>();
        private List<CityTour> savedTours = new List<CityTour>();

        private Dictionary<Journey, Vehicle> journeyDictionary = new Dictionary<Journey, Vehicle>();
        private Dictionary<CityTour, Vehicle> tourDictionary = new Dictionary<CityTour, Vehicle>();

        private Serializer serializer = new Serializer();
        private VehicleHandler vehicleHandler;

        private int journeyId = 1;
        private int tourId = 1;

        public bool savedChanges = true;

        //The constructor uses the serialiazer class to deserialize the files and pass the information into two dictionaries and one list.
        public JourneyHandler()
        {
            journeyDictionary = serializer.deserializeJourney("journeys.xml");
            tourDictionary = serializer.deserializeTour("tours.xml");

            vehicleHandler = new VehicleHandler();

            serializer.closeFiles();
        }

        // This method does the data validation and saves the objects into dictionaries.
        // There are three types for this method.
        // 1 - method sets journey
        // 2 - method sets tour
        // 3 - method edits the selected journey/tour
        public bool setFields(int type, string pickupDescription, string pickupArea, string pickupDate, string destinationDescription, string destinationArea, string customerName, string customerAddress, string comboBoxSelection, string comboBoxVehicle)
        {
            Journey journey = new Journey();
            CityTour cityTour = new CityTour();

            int id = 0;

            if (string.IsNullOrWhiteSpace(pickupDescription))
            {
                MessageBox.Show("Pickup description field is empty!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(pickupArea))
            {
                MessageBox.Show("Pickup area field is empty");
                return false;
            }
            else if (extractAreaCode(pickupArea) == 0)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(destinationDescription))
            {
                MessageBox.Show("Destination Description field is empty");
                return false;
            }

            if (string.IsNullOrWhiteSpace(destinationArea))
            {
                MessageBox.Show("Destination area field is empty");
                return false;
            }
            else if (extractAreaCode(destinationArea) == 0)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(customerName))
            {
                MessageBox.Show("Customer name field is empty");
                return false;
            }

            if (string.IsNullOrWhiteSpace(customerAddress))
            {
                MessageBox.Show("Customer address field is empty");
                return false;
            }

            try
            {
                DateTime.Parse(pickupDate);
            }
            catch
            {
                MessageBox.Show("Please input a valid date MM/DD/YYYY");
                return false;
            }

            if (string.IsNullOrWhiteSpace(comboBoxVehicle))
            {
                MessageBox.Show("Please select a vehicle.");
                return false;
            }


            bool uniqueId;
            int idValidator = 0;
            journeyId = 1;

            if (type == 1)
            {
                // makes sure the id used is unique
                uniqueId = false;
                while (uniqueId == false)
                {
                    if (journeyDictionary.Count == 0)
                    {
                        uniqueId = true;
                        break;
                    }
                    else

                        foreach (Journey jour in journeyDictionary.Keys)
                        {
                            if (jour.JourneyId == journeyId)
                            {
                                idValidator++;
                            }
                        }
                    if (idValidator == 0)
                    {
                        uniqueId = true;
                        idValidator = 0;
                    }
                    else
                    {
                        journeyId++;
                        idValidator = 0;
                    }
                }

                journey = new Journey(
                    pickupDescription, extractAreaCode(pickupArea), DateTime.Parse(pickupDate), destinationDescription, extractAreaCode(destinationArea), customerName, customerAddress, journeyId);

                journeyDictionary.Add(journey, vehicleHandler.getObject(getIdFromString(comboBoxVehicle)));
            }
            else if (type == 2)
            {
                // makes sure the pickup area is the same as the destination area. I used ToUpper to make sure that they are checked even if there is a case difference 
                if (pickupArea.ToUpper() == destinationArea.ToUpper())
                {
                    // makse sure the id used is unique
                    idValidator = 0;
                    uniqueId = false;
                    tourId = 1;
                    while (uniqueId == false)
                    {
                        if (tourDictionary.Count == 0)
                        {
                            uniqueId = true;
                            break;
                        }
                        else

                            foreach (CityTour tour in tourDictionary.Keys)
                            {
                                if (tour.JourneyId == tourId)
                                {
                                    idValidator++;
                                    Console.WriteLine("idValidator: " + idValidator);
                                }
                            }
                        if (idValidator == 0)
                        {
                            uniqueId = true;
                            idValidator = 0;
                        }
                        else
                        {
                            tourId++;
                            idValidator = 0;
                        }
                    }

                    cityTour = new CityTour(
                    pickupDescription, extractAreaCode(pickupArea), DateTime.Parse(pickupDate), destinationDescription, extractAreaCode(destinationArea), customerName, customerAddress, tourId);

                    tourDictionary.Add(cityTour, vehicleHandler.getObject(getIdFromString(comboBoxVehicle)));
                }
                else
                {
                    MessageBox.Show("Pickup area and Destination area must be the same for city tours!");
                    return false;
                }
            }

            // If the type is 3 then the following code runs and modifies the selected entry
            else
            {
                try
                {
                    id = getIdFromString(comboBoxSelection); // gets the id from the string inside the combo box
                }
                catch { MessageBox.Show("Id fault"); }

                //if the combo box contains the word 'tour' then it will modify the tour dictionary
                if (comboBoxSelection.ToUpper().Contains("TOUR"))
                {
                    if (pickupArea.ToUpper() == destinationArea.ToUpper())
                    {
                        cityTour = new CityTour(
                            pickupDescription, extractAreaCode(pickupArea), DateTime.Parse(pickupDate), destinationDescription, extractAreaCode(destinationArea), customerName, customerAddress, id);
                        foreach (CityTour tour in tourDictionary.Keys)
                        {
                            if (tour.JourneyId == cityTour.JourneyId)
                            {
                                cityTour = tour;
                                break;
                            }
                        }
                        //removes the old value and adds a new one with the same ID
                        tourDictionary.Remove(cityTour);
                        tourDictionary.Add(cityTour, vehicleHandler.getObject(getIdFromString(comboBoxVehicle)));
                    }
                    else
                    {
                        MessageBox.Show("Pickup area and Destination area must be the same for city tours!");
                        return false;
                    }

                }
                // else it modifies the journey dictionary
                else
                {
                    journey = new Journey(
                        pickupDescription, extractAreaCode(pickupArea), DateTime.Parse(pickupDate), destinationDescription, extractAreaCode(destinationArea), customerName, customerAddress, id);
                    foreach (Journey jour in journeyDictionary.Keys)
                    {
                        if (jour.JourneyId == journey.JourneyId)
                        {
                            journey = jour;
                        }
                    }

                    journeyDictionary.Remove(journey);
                    journeyDictionary.Add(journey, vehicleHandler.getObject(getIdFromString(comboBoxVehicle)));
                }
            }

            //if the code reaches this part, it mean that everything ran properly and a new journey/tour has been added or modified
            //the savedChanges variable gets set to false to make sure the user gets prompted with a save changes message box
            savedChanges = false;
            return true;

        }

        //returns the journeys dictionary for other classes to use
        public Dictionary<Journey, Vehicle> JourneyDictionary
        {
            get { return journeyDictionary; }
            set { journeyDictionary = value; }
        }
        //returns the tours dictionary for other classes to use
        public Dictionary<CityTour, Vehicle> TourDictionary
        {
            get { return tourDictionary; }
            set { tourDictionary = value; }
        }

        //this method is used to populate the combo box with all the present information from the dictionaries
        public void populateComboBox(ComboBox cmbBoxJourneys, ComboBox cmbBoxVehicles)
        {
            cmbBoxJourneys.Items.Clear();
            cmbBoxVehicles.Items.Clear();
            cmbBoxVehicles.Items.Add(" ");
            cmbBoxVehicles.SelectedItem = " ";

            foreach (Journey journey in journeyDictionary.Keys)
            {
                cmbBoxJourneys.Items.Add(journey.cmbBoxSummary() + " - Journey - Vehicle: " + journeyDictionary[journey].cmbBoxSummary());
            }

            foreach (CityTour tour in tourDictionary.Keys)
            {
                cmbBoxJourneys.Items.Add(tour.cmbBoxSummary() + " - City Tour - Vehicle: " + tourDictionary[tour].cmbBoxSummary());
            }

            foreach (Vehicle vehicle in vehicleHandler.SavedVehicles)
            {
                cmbBoxVehicles.Items.Add(vehicle.cmbBoxSummary());
            }
        }

        //gets the value of the ID from the string that is passed
        public int getIdFromString(string substr)
        {
            string number = "";
            for (int i = 0; i < substr.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(substr.Substring(i, 1)))
                    break;
                number = number + substr.Substring(i, 1);
            }
            try
            {
                return int.Parse(number);
            }
            catch { MessageBox.Show("Fix the id from string problem! " + number); }

            return 0;
        }

        //returns a boolean value depending on the value of the saveChanges variable
        public bool isSaved()
        {
            if (savedChanges)
                return true;
            else
                return false;
        }


        // used for the areas validation. At the same time it extracts the value of the area code from the string
        private int extractAreaCode(string area)
        {
            string s = area.ToUpper();
            string areaCode = "";

            if (s.IndexOf("EH") >= 0)
            {
                for (int i = s.IndexOf("EH") + 2; i < s.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(s.Substring(i, 1)))
                        break;
                    else
                    {
                        areaCode += s.Substring(i, 1);
                    }
                }

                try
                {
                    if (int.Parse(areaCode) < 1 && int.Parse(areaCode) > 22)
                    {
                        MessageBox.Show("The area code should be between EH1 and EH22");
                        return 0;
                    }
                    else
                        return int.Parse(areaCode);
                }
                catch { MessageBox.Show("Incorrect area format! Try something like this: EH11 YTD."); }
            }

            MessageBox.Show("Incorrect area format! Try something like this: EH11 YTD.");
            return 0;
        }
    }
}
