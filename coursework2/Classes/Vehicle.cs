﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2
{
    //This classed is used to create vehicle objects from it.
    //It has two properties and one method
    public class Vehicle
    {
        private int id;
        private string registration;

        public Vehicle() { }
        public Vehicle(int id, string registration)
        {
            this.id = id;
            this.registration = registration;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Registration
        {
            get { return registration; }
            set { registration = value; }
        }

        public string cmbBoxSummary()
        {
            return (id + " - " + registration);
        }
    }
}
