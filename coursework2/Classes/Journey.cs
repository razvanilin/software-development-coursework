﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2.Classes
{
    public class Journey
    {
        //The journey class containing the properties and two methods for calculating the cost and
        //one that returns a string useful for the combo box in the journey window

        private string pickupDescription;
        private int pickupArea;
        private DateTime pickupDate;
        private string destinationDescription;
        private int destinationArea;
        private string customerName;
        private string customerAddress;
        private int journeyId;

        public Journey() { }

        public Journey(
            string pickupDescription, 
            int pickupArea, 
            DateTime pickupDate, 
            string destinationDescription, 
            int destinationArea, 
            string customerName, 
            string customerAddress,
            int id
            )

        {
            this.pickupDescription = pickupDescription;
            this.pickupArea = pickupArea;
            this.pickupDate = pickupDate;
            this.destinationDescription = destinationDescription;
            this.destinationArea = destinationArea;
            this.customerName = customerName;
            this.customerAddress = customerAddress;
            this.journeyId = id;
        }

        public string PickupDescription
        {
            get { return pickupDescription; }
            set { pickupDescription = value; }
        }

        public int PickupArea
        {
            get { return pickupArea; }
            set { pickupArea = value; }
        }

        public DateTime PickupDate
        {
            get { return pickupDate; }
            set { pickupDate = value; }
        }

        public string DestinationDescription
        {
            get { return destinationDescription; }

            set { destinationDescription = value; }
        }

        public int DestinationArea
        {
            get { return destinationArea; }
            set { destinationArea = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public string CustomerAddress
        {
            get { return customerAddress; }
            set { customerAddress = value; }
        }

        public int JourneyId
        {
            get { return journeyId; }
            set { journeyId = value; }
        }


        // returns the cost based on the area code
        public int Cost()
        {
            if (pickupArea == destinationArea)
                return 10;
            if (pickupArea != destinationArea)
                return 20;
            if (pickupArea == 22 || destinationArea == 22)
                return 25;
            return 0;
        }

        //returns a string for the combo box in the journey window 
        public string cmbBoxSummary()
        {
            return (journeyId+ " - "+customerName+" - EH"+ pickupArea+" to EH"+destinationArea);
        }
    }
}
