﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework2
{
    /// <summary>
    /// This is the main window class which will offer the user three buttons which will elad to other windows
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //this is called when the first button is clicked. It will create a new window
        private void btnJourneys_Click(object sender, RoutedEventArgs e)
        {
            Journeys journeys = new Journeys();
            journeys.Show();
        }

        //this is called when the second button is clicked. It will create a new window
        private void btnVehicles_Click(object sender, RoutedEventArgs e)
        {
            VehicleWindow vehicleWindow = new VehicleWindow();
            vehicleWindow.Show();
        }

        //this is called when the last button is clicked. It will create a new window
        private void btnSummary_Click(object sender, RoutedEventArgs e)
        {
            Summary summaryWindow = new Summary();
            summaryWindow.Show();
        }

    }
}
