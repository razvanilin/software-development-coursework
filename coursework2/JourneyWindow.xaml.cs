﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Coursework2.Classes;
using System.Xml.Serialization;
using System.IO;
using System.Collections;

namespace Coursework2  //change the list when you edit an item
{
    /// <summary>
    /// Class used mainly for the visual interaction of the window
    /// </summary>
    public partial class Journeys : Window
    {
        private Serializer serializer;
        private JourneyHandler journeyHandler;

        //the constructor will will call two other constructors and it will populate the combo box using the journey handler
        public Journeys()
        {
            InitializeComponent();

            serializer = new Serializer();
            journeyHandler = new JourneyHandler(); //deserializing the data
            journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle); //populate the combo box using that data
        }

        /* -------------------METHODS----------------------- */

        //A method that clears all the fields in the window
        private void clearFields()
        {
            txtBoxCustomerAddress.Clear();
            txtBoxCustomerName.Clear();
            txtBoxDestinationArea.Clear();
            txtBoxDestinationDescription.Clear();
            txtBoxPickupArea.Clear();
            txtBoxPickupDate.Clear();
            txtBoxPickupDescription.Clear();
        }

        /* -----------------END OF METHODS--------------- */

        /* -------------------EVENTS--------------------- */

        //An event that tells the journey handler to add a journey and set the field to the values from the text boxes
        //if everything went according to the plan, it will clear the fields and then it will populate the combo box with the up-to-date data
        private void btnAddJourney_Click(object sender, RoutedEventArgs e)
        {
            if (journeyHandler.setFields(
                1, 
                txtBoxPickupDescription.Text, 
                txtBoxPickupArea.Text, 
                txtBoxPickupDate.Text, 
                txtBoxDestinationDescription.Text, 
                txtBoxDestinationArea.Text, 
                txtBoxCustomerName.Text, 
                txtBoxCustomerAddress.Text,
                "",
                cmbBoxAddVehicle.SelectedItem.ToString()
                ))
                    clearFields();

            journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle);
        }

        //This method does the same thing as the one above, but it will add a tour instead of a journey
        private void btnAddTour_Click(object sender, RoutedEventArgs e)
        {
            if (journeyHandler.setFields(
                2,
                txtBoxPickupDescription.Text,
                txtBoxPickupArea.Text,
                txtBoxPickupDate.Text,
                txtBoxDestinationDescription.Text,
                txtBoxDestinationArea.Text,
                txtBoxCustomerName.Text,
                txtBoxCustomerAddress.Text,
                "",
                cmbBoxAddVehicle.SelectedItem.ToString()
                ))
                    clearFields();

            journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle);
        }

        //This button is used for testing purposes because it fills the textboxes with data
        private void sampleJourney_Click(object sender, RoutedEventArgs e)
        {
            //Journey sampleJ = new Journey("Broomhouse", "EH11", DateTime.Parse("12/12/2012"), "That street", "eh20", "Raz", "Broomhouse");
            txtBoxPickupDescription.Text = "Broomhouse";
            txtBoxPickupArea.Text = "EH11";
            txtBoxPickupDate.Text = "12/12/2012";
            txtBoxDestinationDescription.Text = "That street";
            txtBoxDestinationArea.Text = "eh20";
            txtBoxCustomerName.Text = "Raz";
            txtBoxCustomerAddress.Text = "Broomhouse";
        }

        //Method that fills the textboxes with valid data for a tour
        private void sampleTour_Click(object sender, RoutedEventArgs e)
        {
            //CityTour sampleT = new CityTour("Broomhouse", "EH11", DateTime.Parse("12/12/2012"), "Gorgie", "eh11", "raz", "Broomhouse");
            txtBoxPickupDescription.Text = "Broomhouse";
            txtBoxPickupArea.Text = "EH11";
            txtBoxPickupDate.Text = "12/12/2012";
            txtBoxDestinationDescription.Text = "Gorgie";
            txtBoxDestinationArea.Text = "eh11";
            txtBoxCustomerName.Text = "raz";
            txtBoxCustomerAddress.Text = "Broomhouse";
        }

        //When the edit button is clicked, this method will get the id from the selected item in the combobox and search for the item inside the dictionary
        //When the journey is found the textboxes will be filled with the object's information
        private void btnEditJourney_Click(object sender, RoutedEventArgs e)
        {
            if (cmbBoxJourneys.SelectedValue!=null)
            {

                int id = journeyHandler.getIdFromString(cmbBoxJourneys.SelectedItem.ToString());//gets the id for the selected item in the combobox          

                if (cmbBoxJourneys.SelectedItem.ToString().ToUpper().Contains("TOUR"))
                {
                    foreach (CityTour tour in journeyHandler.TourDictionary.Keys)
                    {
                        if (tour.JourneyId == id)
                        {
                            txtBoxPickupDescription.Text = tour.PickupDescription;
                            txtBoxPickupArea.Text = "EH"+tour.PickupArea.ToString();
                            txtBoxPickupDate.Text = tour.PickupDate.ToString();
                            txtBoxDestinationDescription.Text = tour.DestinationDescription;
                            txtBoxDestinationArea.Text = "EH"+tour.DestinationArea.ToString();
                            txtBoxCustomerName.Text = tour.CustomerName;
                            txtBoxCustomerAddress.Text = tour.CustomerAddress;
                        }
                    }
                }

                else
                {
                    foreach (Journey journey in journeyHandler.JourneyDictionary.Keys)
                    {
                        if (journey.JourneyId == id)
                        {
                            txtBoxPickupDescription.Text = journey.PickupDescription;
                            txtBoxPickupArea.Text = "EH"+journey.PickupArea.ToString();
                            txtBoxPickupDate.Text = journey.PickupDate.ToString();
                            txtBoxDestinationDescription.Text = journey.DestinationDescription;
                            txtBoxDestinationArea.Text = "EH"+journey.DestinationArea.ToString();
                            txtBoxCustomerName.Text = journey.CustomerName;
                            txtBoxCustomerAddress.Text = journey.CustomerAddress;
                        }
                    }
                }

                btnAddJourney.IsEnabled = false;
                btnAddTour.IsEnabled = false;
                btnCancelEdit.IsEnabled = true;
                btnConfirmEdit.IsEnabled = true;
                btnEditJourney.IsEnabled = false;
                cmbBoxJourneys.IsEnabled = false;
                btnDeleteJourney.IsEnabled = false;
            }

            else
                MessageBox.Show("Select an item first");
        }

        //When the clear button is pressed, the fields will be cleared
        private void btnClear1_Click(object sender, RoutedEventArgs e)
        {
            clearFields();
        }

        //When the confirm edit button is pressed, this method calls the journey handler's method that attributes
        //the information from the textboxes to an object
        private void btnConfirmEdit_Click(object sender, RoutedEventArgs e)
        {
            if (journeyHandler.setFields(
                3,
                txtBoxPickupDescription.Text,
                txtBoxPickupArea.Text,
                txtBoxPickupDate.Text,
                txtBoxDestinationDescription.Text,
                txtBoxDestinationArea.Text,
                txtBoxCustomerName.Text,
                txtBoxCustomerAddress.Text,
                cmbBoxJourneys.SelectedItem.ToString(),
                cmbBoxAddVehicle.SelectedItem.ToString()
                ))
            {
                journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle);
                clearFields();
                btnEditJourney.IsEnabled = true;
                btnAddJourney.IsEnabled = true;
                btnAddTour.IsEnabled = true;
                btnCancelEdit.IsEnabled = false;
                btnConfirmEdit.IsEnabled = false;
                cmbBoxJourneys.IsEnabled = true;
                btnDeleteJourney.IsEnabled = true;
            }

            //journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle);
            
        }

        //This method will reset the window to look like it was at the beginning
        private void btnCancelEdit_Click(object sender, RoutedEventArgs e)
        {
            clearFields();
            btnEditJourney.IsEnabled = true;
            btnAddJourney.IsEnabled = true;
            btnAddTour.IsEnabled = true;
            btnCancelEdit.IsEnabled = false;
            btnConfirmEdit.IsEnabled = false;
            cmbBoxJourneys.IsEnabled = true;
            btnDeleteJourney.IsEnabled = true;
        }

        //This method will serialize all the changes made to the dictionaries
        private void btnSerialize_Click(object sender, RoutedEventArgs e)
        {
            serializer.serializeJourney("journeys.xml", journeyHandler.JourneyDictionary);
            serializer.serializeTour("tours.xml", journeyHandler.TourDictionary);

            journeyHandler.savedChanges = true;
        }

        //this method gets called when a user wants to close the file.
        //if the changes were not saved, a message box will show up reminding the user that the changes were not saved.
        //the box offers three options: Yes - save mthe modifications, No - don't save the modifications, Cancel - return to the window
        private void windowJourney_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!journeyHandler.isSaved())
            {
                MessageBoxResult result = MessageBox.Show("Do you want to save the changes before closing?", string.Empty, MessageBoxButton.YesNoCancel);

                if (result == MessageBoxResult.Yes)
                {
                    serializer.serializeJourney("journeys.xml", journeyHandler.JourneyDictionary);
                    serializer.serializeTour("tours.xml", journeyHandler.TourDictionary);
                }

                else if (result == MessageBoxResult.Cancel)
                    e.Cancel = true;
            }
        }

        //this method gets called when clicking the delete button.
        //it takes the id from the combobox and it deletes that entry from the dicitonary
        private void btnDeleteJourney_Click(object sender, RoutedEventArgs e)
        {
            if (cmbBoxJourneys.SelectedValue != null)
            {
                int id = 0;

                id = journeyHandler.getIdFromString(cmbBoxJourneys.SelectedItem.ToString()); //int.Parse(cmbBoxJourneys.SelectedItem.ToString().Substring(11, 1));
    
                if (cmbBoxJourneys.SelectedItem.ToString().ToUpper().Contains("TOUR"))
                {
                    CityTour removableTour = new CityTour();
                    foreach (CityTour tour in journeyHandler.TourDictionary.Keys)
                    {
                        if (tour.JourneyId == id)
                        {
                            removableTour = tour;
                        }
                    }
                    journeyHandler.TourDictionary.Remove(removableTour);
                    journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle);
                    journeyHandler.savedChanges = false;
                }

                else
                {
                    Journey removable = new Journey();
                    foreach(Journey journey in journeyHandler.JourneyDictionary.Keys)
                    {
                        if (journey.JourneyId == id)
                        {
                            removable = journey;
                            break;
                        }
                    }
                    journeyHandler.JourneyDictionary.Remove(removable);
                    journeyHandler.populateComboBox(cmbBoxJourneys, cmbBoxAddVehicle);
                    journeyHandler.savedChanges = false;
                }
            }
        }
    }
}
