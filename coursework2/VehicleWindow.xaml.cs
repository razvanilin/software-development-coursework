﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework2
{
    /// <summary>
    /// This window class is used to create the visual interactions of the Vehicles window.
    /// </summary>
    public partial class VehicleWindow : Window
    {
        VehicleHandler vehicleHandler;

        //The constructor calls the vehicle handler constructor which deserializes the vehicle data from the file
        //it also populates the combo box with all the existing vehicles
        public VehicleWindow()
        {
            InitializeComponent();

            vehicleHandler = new VehicleHandler();
            vehicleHandler.populateComboBox(cmbBoxVehicleList);
        }

        //when the add vehicle button is clicked, this method uses the vehicle handler object to save the information from the textbox inside the vehicle object
        private void btnAddVehicle_Click(object sender, RoutedEventArgs e)
        {
            vehicleHandler.setFields(txtBoxRegistration.Text);
            vehicleHandler.populateComboBox(cmbBoxVehicleList);
        }

        //This method is used to clear the txt box
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtBoxRegistration.Clear();
        }

        //when the delete button is clicked, the id value will be extracted from the combo box and the object with that id will be removed
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (cmbBoxVehicleList.SelectedValue != null)
            {
                int id = vehicleHandler.getIdFromString(cmbBoxVehicleList.SelectedItem.ToString());

                for (int i = 0; i < vehicleHandler.SavedVehicles.Count; i++)
                {
                    if (vehicleHandler.SavedVehicles[i].Id == id)
                    {
                        vehicleHandler.SavedVehicles.Remove(vehicleHandler.SavedVehicles[i]);
                        i--;
                    }
                }
                vehicleHandler.populateComboBox(cmbBoxVehicleList);
                vehicleHandler.SavedChanges = false;
            }
        }

        //when the user wants to close the window and some modifications were made, a message box appears
        //the box will offer the user three choices: Yes - save the modifications, No - don't save the modifications, Cancel - return to the window
        private void vehicleWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!vehicleHandler.SavedChanges)
            {
                MessageBoxResult result = MessageBox.Show("Do you want to save the changes before closing?", "Notice!", MessageBoxButton.YesNoCancel);

                if (result == MessageBoxResult.Yes)
                {
                    vehicleHandler.serializeVehicle();
                }

                else if (result == MessageBoxResult.Cancel)
                    e.Cancel = true;
            }
        }

        //when the save changes button is clicked, this method will cal the serializer object to serialize all the modifications made
        private void btnSerialize_Click(object sender, RoutedEventArgs e)
        {
            vehicleHandler.serializeVehicle();
            vehicleHandler.SavedChanges = true;
        }
    }
}
