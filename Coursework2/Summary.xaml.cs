﻿//       Author: Razvan Ilin
//Last Modified: 10/12/2013 

using Coursework2.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework2
{
    /// <summary>
    /// The summary window will deserialize all the information about journeys and city tours on load.
    /// It will then display all the information about them in a listbox.
    /// You can have a bit of control over that and you could show journeys and/or tours.
    /// </summary>
    public partial class Summary : Window
    {

        private Serializer serializer = new Serializer();

        private Dictionary<Journey, Vehicle> journeyDictionary;
        private Dictionary<CityTour, Vehicle> tourDictionary;

        // loads everything and deserializes the files
        public Summary()
        {
            InitializeComponent();
            journeyDictionary = serializer.deserializeJourney("journeys.xml");
            tourDictionary = serializer.deserializeTour("tours.xml");
            serializer.closeFiles();
            ckBoxJourneys.IsChecked = true;
            ckBoxTours.IsChecked = true;
        }

        // it calls the 'populateListBox' method when the journeys checkbox is checked or unchecked
        private void ckBoxJourneys_Checked(object sender, RoutedEventArgs e)
        {
            populateListBox();
        }

        private void ckBoxJourneys_Unchecked(object sender, RoutedEventArgs e)
        {
            populateListBox();
        }

        // it calls the 'populateListBox' method when the tours checkbox is checked or unchecked
        private void ckBoxTours_Unchecked(object sender, RoutedEventArgs e)
        {
            populateListBox();
        }

        private void ckBoxTours_Checked(object sender, RoutedEventArgs e)
        {
            populateListBox();
        }

        //Gets called when a checkbox is checked or unchecked and it displays the information from the dictionaries into the list box
        //it populates the listbox depending on the status of the check boxex
        private void populateListBox()
        {
            lstSummary.Items.Clear();

            if (ckBoxJourneys.IsChecked == true)
            {
                foreach (Journey journey in journeyDictionary.Keys)
                {
                    lstSummary.Items.Add(
                        "Journey: " + journey.JourneyId + " - COST: "+journey.Cost()+"£"+
                        " - Customer Name: " + journey.CustomerName + ", Address: " + journey.CustomerAddress +
                        " - From '" + journey.PickupDescription + ", EH" + journey.PickupArea +
                        " - To '" + journey.DestinationDescription + ", EH" + journey.DestinationArea +
                        "' on " + journey.PickupDate +
                        "' - Vehicle ID: " + journeyDictionary[journey].Id + ", Registration: " + journeyDictionary[journey].Registration);
                }
            }

            if (ckBoxTours.IsChecked == true)
            {
                foreach (CityTour tour in tourDictionary.Keys)
                {
                    lstSummary.Items.Add(
                        "City Tour: " + tour.JourneyId + " - COST: "+tour.TourCost()+"£"+
                        " - Customer Name: " + tour.CustomerName + ", Address: " + tour.CustomerAddress +
                        " - From '" + tour.PickupDescription + ", EH" + tour.PickupArea + 
                        " - To '" + tour.DestinationDescription + ", EH" + tour.DestinationArea +
                        "' on " + tour.PickupDate +
                        "' - Vehicle ID: " + tourDictionary[tour].Id + ", Registration: " + tourDictionary[tour].Registration);
                }
            }
        }
    }
}
